#!/bin/bash
pkill vault
START_DIR=$PWD
rm $START_DIR/keys/*.yaml
export VAULT_ADDR='https://127.0.0.1:8200'
./vault server -dev -dev-root-token-id=123 -address="https://127.0.0.1:8200" &
sudo service etcd restart
cd /home/thefish/golang/src/lab.ibcdev.ru/tigra/cli
rm output*.yaml
./cli asset load -r 127.0.0.1:2379 ../exchange/cli/mock/assets_demo.yaml 
./cli symbol load -r 127.0.0.1:2379 ../exchange/cli/mock/symbols_demo.yaml
./cli service load -r 127.0.0.1:2379 ../exchange/cli/mock/services_demo.yaml
./cli prepare -t 123 -e http://127.0.0.1:8200 -i ../exchange/cli/mock/skeleton.yaml 
./cli secrets -t 123 -e  http://127.0.0.1:8200 -i ../exchange/cli/mock/secrets.yaml
mv output*.yaml $START_DIR/keys
echo $START_DIR
cd $START_DIR/updater
./updater.php ~/golang/src/lab.ibcdev.ru/tigra/exchange ../keys
mv ~/golang/src/lab.ibcdev.ru/tigra/exchange/.idea/workspace.xml ~/golang/src/lab.ibcdev.ru/tigra/exchange/.idea/workspace.xml.old
cp tmp.xml ~/golang/src/lab.ibcdev.ru/tigra/exchange/.idea/workspace.xml
cd $START_DIR
