#!/usr/bin/php
<?php

require "vendor/autoload.php";

use CHH\Optparse;
use Symfony\Component\Yaml\Yaml;

$outputFile = "tmp.xml";

$parser = new Optparse\Parser("Ready to work");

$help = function() use ($parser) {
    echo $parser->usage()."\n";
    exit(1);
};

$parser->addFlag("help", ["alias" => "-h"], $help);
$parser->addArgument("exchangeDir", ["required" => true]);
$parser->addArgument("keyDir", ["required" => true]);

try {
    $parser->parse();
} catch (Optparse\Exception $e) {
    $help();
}

unlink($outputFile);

$path = $parser["exchangeDir"];

echo "Looking for exchange project in $path.\n\n";

$fls = scandir($parser["keyDir"], SCANDIR_SORT_DESCENDING);
$keyFile = $parser["keyDir"].DIRECTORY_SEPARATOR.$fls[0];

echo "Loading key config yaml from $keyFile\n\n";

$yaml = Yaml::parse(file_get_contents($keyFile));

$ykeys = array_map(function ($str) { return str_replace('_read','', $str);}, array_column($yaml, "policy"));

$yaml = array_combine($ykeys, $yaml);

//get list of services
$dirs = glob($path . '/*' , GLOB_ONLYDIR);

$services = array_map(
    function ($name) {
        $t = explode(DIRECTORY_SEPARATOR, $name);
        $m = end($t);
        return str_replace("-svc", "", $m);
    },
    array_filter($dirs, function ($name) { return (bool)strpos(strtolower($name), "svc");})
);

if (empty($services)) {
    throw new Exception("No service dirs found in path $exchangeDir");
}

//print_r($services);

//print_r($yaml);

$xml = simplexml_load_file($path."/.idea/workspace.xml");
$xmlFinder = function ($tagName, $attrName, $attrValue) use ($xml) {
    foreach ($xml->{$tagName} as $item) {
        if ((string) $item[$attrName] == $attrValue) {
            return $item;
        }
    }
};

$rm = $xmlFinder('component', 'name', 'RunManager');
//print_r($rm);


$newConfig = function ($service) use ($yaml) :SimpleXMLElement {
    $nc = new SimpleXMLElement('<configuration/>');
    $nc['name'] = 'local '.$service.'-svc.go';
    //type="GoApplicationRunConfiguration" factoryName="Go Application"
    $nc['type'] = 'GoApplicationRunConfiguration';
    $nc['factoryName'] = 'Go Application';
    $nc->module['name'] = 'exchange';
    $nc->working_directory['value'] = '$PROJECT_DIR$/';
    $nc->kind['value'] = 'FILE';
    // <filePath value="$PROJECT_DIR$/account-svc/account-svc.go" />
    $nc->filePath['value'] = '$PROJECT_DIR$/'.$service.'-svc/'.$service.'-svc.go';
    $nc->package['value'] = 'lab.ibcdev.ru/tigra/exchange';
    $nc->directory['value'] = '$PROJECT_DIR$/';
    $nc->method['v'] = '2';
    $nc->parameters['value'] = '-r 127.0.0.1:2379 -v -c -t '.$yaml[$service]['client'].' ';
    return $nc;
};

$toDom = dom_import_simplexml($rm);
$toDom->formatOutput = true;



$toDel = [];
foreach ($rm->configuration as $cfg) {
    if (strpos( " ".(string)$cfg['name'], "local")) {
        echo "\t...deleting ".(string)$cfg['name']."\n";
        array_push($toDel, $cfg);
    }
}

foreach ($toDel as $cfg) {
        //print_r($cfg->asXml());
        $dom=dom_import_simplexml($cfg);
        $dom->formatOutput = true;
        $dom->parentNode->removeChild($dom);
}
//$xml = simplexml_load_string($dom->asXml());
echo "\n\n";
foreach ($services as $s) {
    if (isset($yaml[$s])) {
        $n = $newConfig($s);
        $fromDom = dom_import_simplexml($n);
        $fromDom->formatOutput = true;
        $toDom->appendChild($toDom->ownerDocument->importNode($fromDom, true));

        echo "\t...creating ".$n['name']."\n";
    }
}


$dom = dom_import_simplexml($xml)->ownerDocument;
$dom->formatOutput = true;

file_put_contents($outputFile, $dom->saveXml());
echo "New Goland workspace file saved to ".$outputFile."\n\n";
